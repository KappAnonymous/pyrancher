import os, configparser, shutil,socket, ntpath, json, subprocess, re
from pathlib import Path

class Utils:
    """ Utillity containing various useful functions related to the 
        installation, deployement, config-parsing and such.."""

    def loadConfig(self, configfile):
        try:
            with open(configfile, 'r') as outfile:
                data = json.load(outfile)
                return data
        except Exception as err:
            print(err)
            return None

    def findPackageManager(self):
        """ Function returning the system current package manager
            - apt
            - yum
            - pacman """
        try:
            p = [shutil.which("apt"), shutil.which("yum"), shutil.which("pacman")]
            pm = [value for value in p if value != None]
            return ntpath.basename(pm[0]) if len(pm) == 1 else None
        except Exception as err:
            print(err)
            return None

    def executeScript(self ,USER, SCRIPT):
        """ Execute a system SCRIPT as the specified USER. """
        try:
            return os.system('sudo -H -u %s -S %s' % (USER, SCRIPT))
        except Exception as err:
            print(err)
            return None

    def getInternalIp(self):
        """ Get system internal ip add """
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            res = (s.getsockname()[0])
            s.close()
            return res
        except:
            return '127.0.0.1'

    def getDistro(self):
        """ Get distro name. """
        file = Path("/etc/os-release")
        if file.is_file():
            file = open('/etc/os-release', 'r')
            lines = file.readlines()
            for index, line in enumerate(lines):
                if line.startswith("ID="):
                    file.close()
                    return line.split("=")[1].strip() 
                else:
                    pass
        else:
            return None

        def grepIps(self, clusterfile):
            # opening and reading the file
            with open(clusterfile) as fh:
                fstring = fh.readlines()
            
            # declaring the regex pattern for IP addresses
            pattern = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
            
            # initializing the list object
            lst=[]
            # extracting the IP addresses
            for line in fstring:
                lst.append(pattern.search(line)[0])

    class Ssh:
        # Possible action values : genkey|copyid
        def __init__(self, user=None, password=None, host=None, port=22, keyfile='~/.ssh/id_rsa'):
            self.keyfile = keyfile
            self.host = host
            self.port = port
            self.password = password
            pass

        def key_present(self):
            """Checks to see if there is an RSA already present. Returns a bool."""
            if "id_rsa" in os.listdir(self.keyfile):
                return True
            else:
                return False

        def gen_key(self):
            """Generate a SSH Key."""
            os.chdir(self.keyfile)
            if self.key_present():
                print("A key is already present.")
            else:
                # Genarate private key
                subprocess.call('ssh-keygen -b 2048 -t rsa -f %s -q -N ""' % (self.keyfile), shell=True)

        def push_key(self):
            """Push a SSH Key to a remote server."""
            os.chdir(self.keyfile)
            if self.key_present():
                if "ssh-copy-id" in os.listdir("/usr/local/bin"):
                    print("SSH key found. Pushing key to remote server")
                    command = "echo %s | ssh-copy-id -p %s %s@%s" % (self.password, self.port, self.user, self.host)
                    subprocess.call(command, shell=True)
                else:
                    print("ssh-copy-id required for Mac Users. Use --help for more information.")
            else:
                print("A SSH key is required. Run script again with action set as GenKey")
