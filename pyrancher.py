#!/usr/bin/python3

import os, sys, socket, configparser, shutil, ntpath, json, argparse, utils, subprocess

PWD=os.path.dirname(os.path.realpath(__file__))
utility = utils.Utils()

class _Rancher:
    """ Rancher server object.
        properties :
            - distro : Distro of the host system.
            - domainsNames : Public ip or url where the cluster can be reached.
            - token : if not a new cluster, provide the an auth token to join the node to a pre-existing cluster.
            - installName : Can be rke, rke2, ks3.
            - internalIp : Internal ip. Used in the intra cluster nodes communication."""

    def __init__(self):
        self.internalIp = utility.getInternalIp()
        self.packageManager = utility.findPackageManager()
        self.distro = utility.getDistro().lower() # replace 'Ubuntu'.lower() for utility.getDistro().lower() when tests are done.

class Dependencies:
    """ Initialize an array of dependencies:Type:dependency.
        According to the installName provided in the Rancher object. """
    def __getDependencies(self, rancher):
        config = utility.loadConfig("{}/config.json".format(PWD))
        dependencies = config[rancher.installName]['dependencies'][rancher.packageManager] # replace 'apt' for rancher.packageManager when tests are done.
        deps=[]
        for dep in dependencies: 
            deps.append(self.Dependency(dependencies[dep]['install-script']['runas'], dependencies[dep]['install-script']['exec']))
        return deps

    def __init__(self, rancher):
        self.dependencies = self.__getDependencies(rancher)

    class Dependency:
        """ Structured representation of a dependency.
            - user = The user the script will run as.
            - script = Script to execute."""
        def __init__(self, user, script):
            self.user = user
            self.script = script

class Rke(_Rancher):
    """ Object containing every information needed to start using Rancher. """
    def __init__(self):
        super(). __init__()
        self.installName = 'rke'
        self.dependencies  = Dependencies(self)
        
    def _installDependencies(self):
        for dep in self.dependencies.dependencies:
            for line in dep.script:
                if utility.executeScript(dep.user, line) != None:
                    print("Dependencies installed.")
                else:
                    print("Error during dependencies installation.")

    def install(self):
        print("Installing deployement dependencies...")
        self._installDependencies()
        print("Exporting SSH key to all the nodes...")
        pass


def start():
    parser = argparse.ArgumentParser(description='This is a python3 rancher server installer.')
    config_pars = parser.add_argument_group('Script configuration')
    config_pars.add_argument('-a','--action', choices=['install', 'update'], help='Script\'s ACTION to do: install|update', required=True)
    config_pars.add_argument('-t','--type', choices=['rke', 'rke2', 'k3s'], help='Rancher deployment TYPE.', required=True)
    args = parser.parse_args()
    if args.type.lower() == 'rke':
        rke = Rke()
        rke.install()
