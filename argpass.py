import os, sys, socket, configparser, shutil, ntpath, json, argparse, utils, subprocess

parser = argparse.ArgumentParser(description='This is a python3 rancher server installer.')
config_pars = parser.add_argument_group('Script configuration')
config_pars.add_argument('-a','--action', choices=['install', 'update'], help='Script\'s ACTION to do: install|update', required=True)
config_pars.add_argument('-t','--type', choices=['rke', 'rke2', 'k3s'], help='Rancher deployment TYPE.', required=True)
config_pars.add_argument('-c','--configfile', help='Path to the cluster.yaml file.', required=True)
config_pars.add_argument('-s','--ssh', dest='ssh', action='store_false', help='Turn on if the script should generate non-existant ssh-key in the cluster.yaml file. Default is YES.', required=False, default=True)
args = parser.parse_args()

print ("Selected installation action: %s" % args.action.lower())
print ("Selected installation type: %s" % args.type.lower())
print ("Selected installation configfile: %s" % args.configfile.lower())
print ("Selected installation ssh: %s" % args.ssh)